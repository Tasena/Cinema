
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>EditSessions</title>
    <link rel="stylesheet" type="text/css" href="css/movies.css">
</head>
<body>
<form action="/logout">
    <h2 align="right">${user.firstName}&nbsp;${user.lastName}</h2>
    <p align="right"><input type="submit" class="new" name="logout" value="Logout"/></p><hr>
</form>
<h2>${movieTitle}</h2>
<form method="get" action="/createSession">
    <p><input type="hidden" name="movieId" value="${movie.id}"/></p>
    <p><input type="submit" class="new" name="create" value="Create session"/></p></form>

    <c:forEach items="${sessionsList}" var="session">
        <h2><font color="black" face="Times new roman"><fmt:formatDate value="${session.sessionDate}" pattern="dd-MM-yyyy HH:mm"/></font></h2>

        <form method="post" action="/deleteSession">
            <p><input type="hidden" name="id" value="${session.id}"/></p>
            <p><input type="submit" class="new" name="delete" value="Delete session"/></p></form>
        <form method="post" action="/editSession">
            <p><input type="hidden" name="id" value="${session.id}"/></p>
            <p><input type="hidden" name="id" value="${movie.id}"/></p>
            <p><input type="submit" class="new" name="edit" value="Edit session"/></p></form>
        <hr>
    </c:forEach>

</form>
</body>
</html>
