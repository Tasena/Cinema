
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>EditMovie</title>
    <link rel="stylesheet" type="text/css" href="css/movies.css">
</head>
<body>
<form action="/logout">
    <h2 align="right">${user.firstName}&nbsp;${user.lastName}</h2>
    <p align="right"><input type="submit" class="new" name="logout" value="Logout"/></p><hr>
</form>
<form action="/createMovie.jsp">
    <p><input type="submit" class="new" name="createMovie" value="Create movie"/></p>
    <hr>
</form>

<c:forEach items="${moviesList}" var="movie">
        <h2 align="center"><font color="black" face="Times new roman">${movie.title}</font></h2>
        <p><font size="4" color="black" face="Times new roman">Duration&nbsp;&nbsp;${movie.duration}&nbsp;min.&nbsp;&nbsp;
            Genre&nbsp;:&nbsp;${movie.genre}</p>
        <p><font size="4" color="black" face="Times new roman">Description&nbsp;:&nbsp;${movie.description}</p>

    <form method="post" action="/editSession">
        <p><input type="hidden" name="id" value="${movie.id}"/></p>
        <p><input type="submit" class="new" name="session" value="Edit sessions for movie"/></p></form>
    <form method="post" action="/editMovie">
        <p><input type="hidden" name="id" value="${movie.id}"/></p>
        <p><input type="submit" class="new" name="editMovie" value="Edit movie"/></p></form>
    <form method="post" action="/deleteMovie">
        <p><input type="hidden" name="id" value="${movie.id}"/></p>
        <p><input type="submit" class="new" name="deleteMovie" value="Delete movie"/></p></form>
    <hr>
</c:forEach>
</body>
</html>
