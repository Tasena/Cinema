
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>NoSession</title>
    <link rel="stylesheet" type="text/css" href="css/movies.css">
</head>
<body>

<form action="/moviesList">
    <h2 align="center"><font color="black" face="Times new roman">No sessions for this movie</font></h2>
    <p><input type="submit" class="new" name="ticket" value="Back to movies List"/></p>
    </form>
</body>
</html>
