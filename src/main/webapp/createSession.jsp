
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>CreateSession</title>
    <link rel="stylesheet" type="text/css" href="css/movies.css">
</head>
<body>
<form action="/logout">
    <h2 align="right">${user.firstName}&nbsp;${user.lastName}</h2>
    <p align="right"><input type="submit" class="new" name="logout" value="Logout"/></p><hr>
</form>

<form method="post" action="/createSession">
    <p><font size="4" color="#2F4F4F" face="Times new roman">${}</font></p>
    <p><font size="4" color="#2F4F4F" face="Times new roman">Date&nbsp;:&nbsp;</font>
        <input type="text" class="colortext" name="title" value=""/></p>
    <p><font size="4" color="#2F4F4F" face="Times new roman">Hall&nbsp;:&nbsp;</font>
        <input type="text" class="colortext" name="hall" value="" /></p>
    <p><font size="4" color="#2F4F4F" face="Times new roman">Duration&nbsp;:&nbsp;</font>
        <input type="text" class="colortext" name="duration" value=""/></p>
    <p><font size="4" color="#2F4F4F" face="Times new roman">Description&nbsp;:&nbsp;</font></p>
    <textarea class="colortext" name="description" cols="150" rows="5"></textarea>

    <p align="center"><input type="submit" class="new" name="create" value="Create movie"/></p>
</form>
</body>
</html>
