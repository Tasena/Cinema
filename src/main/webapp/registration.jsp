
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Registration</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>css/styles.css">
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/registration">
    <div align="center">
        <div class="newopacity">
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">First name : </font>
                <input type="text" class="colortext" name="firstName" value="${fn:escapeXml(param.firstName)}" /></p>
            <span class="colortext1">${messages.firstName}</span>
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">Last name : </font>
                <input type="text" class="colortext" name="lastName" value="${fn:escapeXml(param.lastName)}" /></p>
            <span class="colortext1">${messages.lastName}</span>
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">Login : </font>
                <input type="text" class="colortext" name="login" value="${fn:escapeXml(param.login)}" /></p>
            <span class="colortext1">${messages.login}</span>
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">Password : </font>
                <input type="password" class="colortext" name="password" value="${fn:escapeXml(param.password)}" /></p>
            <span class="colortext1">${messages.password}</span>
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">Birthday : </font>
                <input type="date" class="colortext" name="birthday" value="${fn:escapeXml(param.birthday)}" /></p>
            <span class="colortext1">${messages.birthday}</span>
            <p align="center"><font size="5" color="#2F4F4F" face="Times new roman">Email : </font>
                <input type="text" class="colortext" name="email" value="${fn:escapeXml(param.email)}" /></p>
            <span class="colortext1">${messages.email}</span>
            <p align="center"><input type="submit" class="colortext1" name="registrater" value="Register"/></p>
        </div>
    </div>
</form>
    <form action="login">
        <div align="center">
            <div class="newopacity">
                <p><font size="5" color="#2F4F4F" face="Times new roman">Already registered : </font>
                    <input type="submit" class="colortext1" name="login" value="Log in"/></p>
            </div>
        </div>
</form>
</body>
</html>

