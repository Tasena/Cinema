package controller;

import dto.UserDto;


import model.UserRole;
import service.api.UserService;
import service.impl.UserServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            UserDto user = new UserDto();
            UserService userService = UserServiceImpl.getInstance();
            user = userService.findUserByLoginPassword(login, password);

            if(user == null) {
                response.sendRedirect("wrongLogin.jsp");
            } else {
                if(user.getRole().equals(UserRole.ADMIN)) {
                    request.getSession().setAttribute("user", user);
                    response.sendRedirect("editMovie");
                } else {
                    request.getSession().setAttribute("user", user);
                    response.sendRedirect("moviesList");
                }
            }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);

    }
}
