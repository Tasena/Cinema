package controller;


import dto.MovieDto;
import service.impl.MovieServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@WebServlet("/deleteMovie")
public class DeleteMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer movieId = Integer.valueOf(request.getParameter("id"));
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        MovieDto movieDto = movieService.findById(movieId);
        movieService.delete(movieId);
        response.sendRedirect("/editMovie");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
