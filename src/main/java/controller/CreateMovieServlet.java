package controller;

import dto.MovieDto;
import service.impl.MovieServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/createMovie")
public class CreateMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        MovieDto movieDto = new MovieDto();
        movieDto.setTitle(request.getParameter("title"));
        movieDto.setGenre(request.getParameter("genre"));
        movieDto.setDuration(Integer.valueOf(request.getParameter("duration")));
        movieDto.setDescription(request.getParameter("description"));
        movieService.create(movieDto);
        response.sendRedirect("/editMovie");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer movieId = Integer.valueOf(request.getParameter("id"));
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        MovieDto movieDto = movieService.findById(movieId);
        movieDto.setId(movieId);
        movieDto.setTitle(request.getParameter("title"));
        movieDto.setGenre(request.getParameter("genre"));
        movieDto.setDuration(Integer.valueOf(request.getParameter("duration")));
        movieDto.setDescription(request.getParameter("description"));
        movieService.update(movieDto);
        response.sendRedirect("/editMovie");
    }
}
