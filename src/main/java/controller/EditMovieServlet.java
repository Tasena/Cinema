package controller;

import dto.MovieDto;
import service.api.MovieService;
import service.impl.MovieServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/editMovie")
public class EditMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer movieId = Integer.valueOf(request.getParameter("id"));
        MovieDto movie = new MovieDto();
        MovieService movieService = MovieServiceImpl.getInstance();
        movie = movieService.findById(movieId);
        request.getSession().setAttribute("movie", movie);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/updateMovie.jsp");
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MovieService movieService = MovieServiceImpl.getInstance();
        List<MovieDto> moviesList = movieService.findAll();
        request.getSession().setAttribute("moviesList", moviesList);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/editMovie.jsp");
        rd.forward(request, response);
    }
}
