package controller;

import dto.MovieDto;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/createSession")
public class CreateSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String movieId = request.getParameter("movieId");
        Integer id = Integer.valueOf(movieId);
        MovieDto movieDto = new MovieDto();
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        movieDto = movieService.findById(id);
        request.setAttribute("title", movieDto.getTitle());
        response.sendRedirect("/createSession.jsp");

    }
}
