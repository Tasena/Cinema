package controller;

import dto.UserDto;
import model.UserRole;
import service.api.UserService;
import service.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@WebServlet("/registration")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Map<String, String> messeges = new HashMap<>();
        request.setAttribute("messeges", messeges);
        String firstName = request.getParameter("firstName");
        if(!checkName(firstName)) {
            messeges.put("firstName", "Name has to begin with a capital letter and the remaining letters mast be in lowercase.");
        }
        String lastName = request.getParameter("lastName");
        if(!checkName(lastName)) {
            messeges.put("lastName", "Name has to begin with a capital letter and the remaining letters mast be in lowercase.");
        }
        String login = request.getParameter("login");
        if(login.length() < 5 && checkLogin(login)) {
            messeges.put("login", "Login is too short or already used.");
        }
        String password = request.getParameter("password");
        if(password.length() < 5) {
            messeges.put("password", "Password is too short.");
        }

        LocalDate birthday = null;
        if((request.getParameter("birthday") != null)) {
            birthday = LocalDate.parse(request.getParameter("birthday"));
            if(checkBirthday(birthday)) {
                messeges.put("birthday", "Your age mast be > 18 and < 100");
            }
        } else {
            messeges.put("birthday", "Input your birthday");
        }

        String email = request.getParameter("email");
        if(!checkEmale(email)) {
            messeges.put("email", "Wrong email.");
            request.setAttribute("messages", messeges);
            request.getRequestDispatcher("registration.jsp").forward(request, response);
        }

        UserDto userDto = new UserDto();
        userDto.setFirstName(firstName);
        userDto.setLastName(lastName);
        userDto.setLogin(login);
        userDto.setPassword(password);
        userDto.setBirthday(birthday);
        userDto.setEmail(email);
        userDto.setRole(UserRole.DEFAULT);

        UserService userServise = UserServiceImpl.getInstance();
        userServise.create(userDto);

        RequestDispatcher dispatcher = request.getRequestDispatcher("registration.jsp");
        request.getRequestDispatcher("login.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

    public boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^([A-Z][a-z]{1,15})");
        Matcher matcher = pattern.matcher(name);
        return matcher.find();
    }

    public boolean checkEmale(String emale) {
        Pattern pattern = Pattern.compile(".{1,20}@.{1,15}\\.[com|ru|ua]");
        Matcher matcher = pattern.matcher(emale);
        return matcher.find();
    }

    public boolean checkBirthday(LocalDate birthday) {
        LocalDate today = LocalDate.now();
        return !((birthday.isBefore(today.minusYears(18)) && birthday.isAfter(today.minusYears(100))));

    }

    public boolean checkLogin(String login) {
        UserService userService = UserServiceImpl.getInstance();
        List<UserDto> userDtos = userService.findAll();
        for (UserDto userDto : userDtos) {
            if (userDto.getLogin().equals(login)) return true;
        }
        return false;
    }
}
