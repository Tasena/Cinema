package controller;

import dto.MovieDto;
import service.api.MovieService;
import service.impl.MovieServiceImpl;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/moviesList")
public class MoviesListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MovieService movieService = MovieServiceImpl.getInstance();
        List<MovieDto> moviesList = movieService.findAll();
        request.getSession().setAttribute("moviesList", moviesList);
        response.sendRedirect("/moviesList.jsp");
    }
}
