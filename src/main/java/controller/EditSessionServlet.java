package controller;

import dto.MovieDto;
import dto.SessionDto;
import service.api.SessionService;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/editSession")
public class EditSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer movieId = Integer.valueOf(request.getParameter("id"));
        MovieDto movieDto = new MovieDto();
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        movieDto = movieService.findById(movieId);
        request.setAttribute("movieTitle", movieDto.getTitle());
        SessionService sessionService = SessionServiceImpl.getInstance();
        List<SessionDto> sessionsList = sessionService.findAllSessionsByMovieId(movieId);
        if(!sessionsList.isEmpty()) {
            request.getSession().setAttribute("sessionsList", sessionsList);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/editSessions.jsp");
            rd.forward(request, response);
        } request.getRequestDispatcher("/noSuchSession.jsp").forward(request, response);
    }
}


