package dto;

import java.io.Serializable;

public class TicketDto implements Serializable {

    private Integer id;
    private Integer place;
    private Integer row;
    private UserDto userDto;
    private SessionDto sessionDto;

    public TicketDto() {
    }

    public TicketDto(Integer id, Integer place, Integer row, UserDto userDto, SessionDto sessionDto) {
        setId(id);
        setPlace(place);
        setRow(row);
        setUserDto(userDto);
        setSessionDto(sessionDto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketDto ticketDto = (TicketDto) o;

        if (!getId().equals(ticketDto.getId())) return false;
        if (!getPlace().equals(ticketDto.getPlace())) return false;
        if (!getRow().equals(ticketDto.getRow())) return false;
        if (!getUserDto().equals(ticketDto.getUserDto())) return false;
        return getSessionDto().equals(ticketDto.getSessionDto());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getPlace().hashCode();
        result = 31 * result + getRow().hashCode();
        result = 31 * result + getUserDto().hashCode();
        result = 31 * result + getSessionDto().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "id=" + id +
                ", place=" + place +
                ", row=" + row +
                ", userDto=" + userDto +
                ", sessionDto=" + sessionDto +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public SessionDto getSessionDto() {
        return sessionDto;
    }

    public void setSessionDto(SessionDto sessionDto) {
        this.sessionDto = sessionDto;
    }
}
