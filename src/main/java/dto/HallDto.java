package dto;

public class HallDto {

    private Integer id;
    private String name;
    private Integer rowCount;
    private Integer placeCount;

    public HallDto() {
    }

    public HallDto(Integer id, String name, Integer rowCount, Integer placeCount) {
        setId(id);
        setName(name);
        setRowCount(rowCount);
        setPlaceCount(placeCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HallDto hall = (HallDto) o;
        return getId().equals(hall.getId())&&getName().equals(hall.getName()) &&
                getRowCount().equals(rowCount) && getPlaceCount().equals(placeCount);

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getRowCount().hashCode();
        result = 31 * result + getPlaceCount().hashCode();
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Integer getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(Integer placeCount) {
        this.placeCount = placeCount;
    }
}
