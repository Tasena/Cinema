package dto;

import java.util.Date;
import java.util.List;

public class SessionDto {

    private Integer id;
    private MovieDto movieDto;
    private HallDto hallDto;
    private Date sessionDate;

    public SessionDto() {
    }

    public SessionDto(Integer id, MovieDto movieDto, HallDto hallDto, Date sessionDate) {
        setId(id);
        setMovieDto(movieDto);
        setHallDto(hallDto);
        setSessionDate(sessionDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionDto that = (SessionDto) o;

        return getId().equals(that.getId()) && getMovieDto().equals(that.getMovieDto()) &&
                getHallDto().equals(that.getHallDto())&& getSessionDate().equals(that.getSessionDate());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getMovieDto().hashCode();
        result = 31 * result + getHallDto().hashCode();
        result = 31 * result + getSessionDate().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SessionDto{" +
                "id=" + id +
                ", movieDto=" + movieDto +
                ", hallDto=" + hallDto +
                ", sessionDate=" + sessionDate +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MovieDto getMovieDto() {
        return movieDto;
    }

    public void setMovieDto(MovieDto movieDto) {
        this.movieDto = movieDto;
    }

    public HallDto getHallDto() {
        return hallDto;
    }

    public void setHallDto(HallDto hallDto) {
        this.hallDto = hallDto;
    }

    public Date getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(Date sessionDate) {
        this.sessionDate = sessionDate;
    }

}
