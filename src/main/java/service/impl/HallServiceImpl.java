package service.impl;


import dao.api.HallDao;
import dao.impl.HallDaoInMemoryImpl;
import dao.impl.HallDaoSqlImpl;
import dto.HallDto;
import model.Hall;
import propertyHolder.PropertyHolder;
import service.Transformer;
import service.api.HallService;
import java.util.List;

public final class HallServiceImpl implements HallService {

    private static HallServiceImpl hallService;

    public synchronized static HallServiceImpl getInstance() {
        if (hallService == null) {
            hallService = new HallServiceImpl();
        }
        return hallService;
    }

    public HallServiceImpl() {
    }

    public void create(HallDto hallDto) {
        HallDao hallDao = null;
        if(!PropertyHolder.getInstance().isDB()) {
            hallDao = HallDaoInMemoryImpl.getInstance();

        } else {
            hallDao = HallDaoSqlImpl.getInstance();
        }
        hallDao.create(Transformer.hallDtoToHall(hallDto));
    }


    public void delete(Integer id) {
        if(!PropertyHolder.getInstance().isDB()) {
            HallDao hallDao = HallDaoInMemoryImpl.getInstance();
            hallDao.delete(id);
        } else {
            HallDao hallDao = HallDaoSqlImpl.getInstance();
            hallDao.delete(id);
        }
    }

    public List findAll() {
        HallDao hallDao;
        if(!PropertyHolder.getInstance().isDB()) {
            hallDao = HallDaoInMemoryImpl.getInstance();
        } else {
            hallDao = HallDaoSqlImpl.getInstance();
        }
        List<Hall> halls = hallDao.findAll();
        List<HallDto> hallDtos = Transformer.listHallsToListHallsDto(halls);
        return hallDtos;
    }

    public HallDto update(HallDto hallDto) {
        HallDao hallDao;
        if(!PropertyHolder.getInstance().isDB()) {
            hallDao = HallDaoInMemoryImpl.getInstance();
        } else {
            hallDao = HallDaoSqlImpl.getInstance();
        }
        Hall hall = (Hall) hallDao.update(Transformer.hallDtoToHall(hallDto));
        HallDto hallDto1 = Transformer.hallToHallDto(hall);
        return hallDto1;
    }

    public HallDto findById(Integer id) {
        HallDao hallDao;
        if(!PropertyHolder.getInstance().isDB()) {
            hallDao = HallDaoInMemoryImpl.getInstance();
        } else {
            hallDao = HallDaoSqlImpl.getInstance();
        }
        Hall hall = (Hall)hallDao.findById(id);
        HallDto hallDto = Transformer.hallToHallDto(hall);
        return hallDto;
    }
}
