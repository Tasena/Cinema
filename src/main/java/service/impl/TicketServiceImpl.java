package service.impl;


import dao.api.TicketDao;
import dao.impl.TicketDaoInMemoryImpl;
import dao.impl.TicketDaoSqlImpl;
import dto.TicketDto;
import model.Ticket;
import propertyHolder.PropertyHolder;
import service.Transformer;
import service.api.TicketService;

import java.util.List;

public final class TicketServiceImpl implements TicketService{

    private static TicketServiceImpl ticketService;

    public TicketServiceImpl() {
    }

    private synchronized static TicketServiceImpl getInstance() {
        if(ticketService == null) {
            ticketService = new TicketServiceImpl();
        }
        return ticketService;
    }

    @Override
    public void create(TicketDto ticketDto) {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
        }
        ticketDao.create(Transformer.ticketDtoToTicket(ticketDto));
    }

    @Override
    public void delete(Integer id) {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
            ticketDao.delete(id);
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
            ticketDao.delete(id);
        }
    }

    @Override
    public List<TicketDto> findAll() {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
        }
        List<Ticket> tickets = ticketDao.findAll();
        return Transformer.listTicketToListTocketDto(tickets);
    }

    @Override
    public TicketDto update(TicketDto ticketDto) {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
        }
        Ticket ticket = (Ticket) ticketDao.update(Transformer.ticketDtoToTicket(ticketDto));
        return Transformer.ticketToTicketDto(ticket);
    }

    @Override
    public TicketDto findById(Integer id) {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
        }
        Ticket ticket = (Ticket) ticketDao.findById(id);
        return Transformer.ticketToTicketDto(ticket);
    }

    @Override
    public void deleteAllTicketsBySessionId(Integer sessionId) {
        TicketDao ticketDao;
        if(!PropertyHolder.getInstance().isDB()) {
            ticketDao = TicketDaoInMemoryImpl.getInstance();
            ticketDao.deleteAllTicketsBySessionId(sessionId);
        } else {
            ticketDao = TicketDaoSqlImpl.getInstance();
            ticketDao.deleteAllTicketsBySessionId(sessionId);
        }
    }

    @Override
    public List<TicketDto> findAllTicketsBySessionId(Integer sessionId) {
            TicketDao ticketDao;
            if(!PropertyHolder.getInstance().isDB()) {
                ticketDao = TicketDaoInMemoryImpl.getInstance();
            } else {
                ticketDao = TicketDaoSqlImpl.getInstance();
            }
            List<Ticket> tickets = ticketDao.findAllTicketsBySessionId(sessionId);
            return Transformer.listTicketToListTocketDto(tickets);
    }
}
