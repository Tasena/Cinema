package service.impl;


import dao.api.MovieDao;
import dao.impl.MovieDaoInMemoryImpl;
import dao.impl.MovieDaoSqlImpl;
import dto.MovieDto;
import model.Movie;
import propertyHolder.PropertyHolder;
import service.Transformer;
import service.api.MovieService;

import java.util.List;

public final class MovieServiceImpl implements MovieService{

    private static MovieServiceImpl movieService;

    public synchronized static MovieServiceImpl getInstance() {
        if(movieService == null) movieService = new MovieServiceImpl();
        return movieService;
    }

    public MovieServiceImpl() {
    }

    @Override
    public void create(MovieDto movieDto){
        MovieDao movieDao;
        if(!PropertyHolder.getInstance().isDB()) {
            movieDao = MovieDaoInMemoryImpl.getInstance();
        } else {
            movieDao = MovieDaoSqlImpl.getInstance();
        }
        movieDao.create(Transformer.movieDtoToMovie(movieDto));
    }

    @Override
    public void delete(Integer id) {
        if(!PropertyHolder.getInstance().isDB()) {
            MovieDao movieDao = MovieDaoInMemoryImpl.getInstance();
            movieDao.delete(id);
        } else {
            MovieDao movieDao = MovieDaoSqlImpl.getInstance();
            movieDao.delete(id);
        }

    }

    @Override
    public List<MovieDto> findAll() {
        MovieDao movieDao;
        if(!PropertyHolder.getInstance().isDB()) {
            movieDao = MovieDaoInMemoryImpl.getInstance();
        } else {
            movieDao = MovieDaoSqlImpl.getInstance();
        }
        List<Movie> movies = movieDao.findAll();
        List<MovieDto> movieDtos = Transformer.listMovieToListMovieDTO(movies);
        return movieDtos;
    }

    @Override
    public MovieDto update(MovieDto movieDto) {
        MovieDao movieDao;
        if(!PropertyHolder.getInstance().isDB()) {
            movieDao = MovieDaoInMemoryImpl.getInstance();
        } else {
            movieDao = MovieDaoSqlImpl.getInstance();
        }
        Movie movie = (Movie)movieDao.update(Transformer.movieDtoToMovie(movieDto));
        MovieDto movieDto1 = Transformer.movieToMovieDto(movie);
        return movieDto1;
    }

    @Override
    public MovieDto findById(Integer id) {
        MovieDao movieDao;
        if(!PropertyHolder.getInstance().isDB()) {
            movieDao = MovieDaoInMemoryImpl.getInstance();
        } else {
            movieDao = MovieDaoSqlImpl.getInstance();
        }
        Movie movie = (Movie) movieDao.findById(id);
        MovieDto movieDto = Transformer.movieToMovieDto(movie);
        return movieDto;
    }
}
