package service.impl;


import dao.api.UserDao;
import dao.impl.UserDaoInMemoryImpl;
import dao.impl.UserDaoSqlImpl;
import dto.UserDto;
import model.User;
import propertyHolder.PropertyHolder;
import service.Transformer;
import service.api.UserService;
import java.util.List;

public final class UserServiceImpl implements UserService{

    private static UserServiceImpl userService;

    public UserServiceImpl() {
    }

    public synchronized static UserServiceImpl getInstance() {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }

    @Override
    public void create(UserDto userDto){

        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
        } else {
            userDao = UserDaoSqlImpl.getInstance();
        }
        userDao.create(Transformer.userDtoToUser(userDto));
    }

    @Override
    public void delete(Integer id) {
        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
            userDao.delete(id);
        } else {
            userDao = UserDaoSqlImpl.getInstance();
            userDao.delete(id);
        }
    }

    @Override
    public List<UserDto> findAll(){
        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
        } else {
            userDao = UserDaoSqlImpl.getInstance();
        }
           List<User> users = userDao.findAll();
        return Transformer.listUserToListUserDto(users);
    }

    @Override
    public UserDto update(UserDto userDto) {
        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
        } else {
          userDao = UserDaoSqlImpl.getInstance();

        }
        User user = Transformer.userDtoToUser(userDto);
        return Transformer.userToUserDto((User) userDao.update(user));
    }

    @Override
    public UserDto findById(Integer id) {
        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
        } else {
            userDao = UserDaoSqlImpl.getInstance();
        }
        return Transformer.userToUserDto((User) userDao.findById(id));
    }

    @Override
    public UserDto findUserByLoginPassword(String login, String password) {
        UserDao userDao;
        if(!PropertyHolder.getInstance().isDB()) {
            userDao = UserDaoInMemoryImpl.getInstance();
        } else {
            userDao = UserDaoSqlImpl.getInstance();
        }
        return Transformer.userToUserDto(userDao.findUserByLoginPassword(login, password));
    }
}
