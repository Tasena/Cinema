package service.impl;

import dao.api.SessionDao;
import dao.impl.SessionDaoInMemoryImpl;
import dao.impl.SessionDaoSqlImpl;
import dto.SessionDto;
import model.Session;
import propertyHolder.PropertyHolder;
import service.Transformer;
import service.api.SessionService;

import java.util.List;

public final class SessionServiceImpl implements SessionService{

    private static SessionServiceImpl sessionService;

    public SessionServiceImpl() {
    }

    public synchronized static SessionServiceImpl getInstance() {
        if(sessionService == null) {
            sessionService = new SessionServiceImpl();
        }
        return sessionService;
    }

    @Override
    public void create(SessionDto sessionDto) {

        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
        }
        sessionDao.create(Transformer.sessionDtoToSession(sessionDto));
    }

    @Override
    public void delete(Integer id) {
        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
            sessionDao.delete(id);
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
            sessionDao.delete(id);
        }
    }

    @Override
    public List<SessionDto> findAll() {
        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
        }
        List<Session> sessions = sessionDao.findAll();
        return Transformer.listSessionToListSessionDto(sessions);
    }

    @Override
    public SessionDto update(SessionDto sessionDto) {
        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
        }
        Session session = (Session) sessionDao.update(Transformer.sessionDtoToSession(sessionDto));
        return Transformer.sessionToSessionDto(session);
    }

    @Override
    public SessionDto findById(Integer id) {
        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
        }
        return Transformer.sessionToSessionDto((Session) sessionDao.findById(id));
    }

    @Override
    public List<SessionDto> findAllSessionsByMovieId(Integer movieId) {
        SessionDao sessionDao;
        if(!PropertyHolder.getInstance().isDB()) {
            sessionDao = SessionDaoInMemoryImpl.getInstance();
        } else {
            sessionDao = SessionDaoSqlImpl.getInstance();
        }
        List<Session> sessions = sessionDao.findAllSessionsByMovieId(movieId);
        return Transformer.listSessionToListSessionDto(sessions);
    }
}
