package service.api;

import dto.UserDto;
import java.util.List;

public interface UserService {

    void create (UserDto userDto);
    void delete(Integer id);
    List<UserDto> findAll();
    UserDto update(UserDto userDto);
    UserDto findById(Integer id);
    UserDto findUserByLoginPassword(String login, String password);
}
