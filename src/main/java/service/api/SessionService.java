package service.api;

import dto.SessionDto;

import java.util.List;

public interface SessionService {
    void create (SessionDto sessionDto);
    void delete(Integer id);
    List<SessionDto> findAll();
    SessionDto update(SessionDto sessionDto);
    SessionDto findById(Integer id);
    List<SessionDto> findAllSessionsByMovieId(Integer movieId);
}
