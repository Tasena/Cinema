package service.api;

import dto.MovieDto;

import java.util.List;

public interface MovieService {
    void create (MovieDto movieDto);
    void delete(Integer id);
    List<MovieDto> findAll();
    MovieDto update(MovieDto movieDto);
    MovieDto findById(Integer id);
}
