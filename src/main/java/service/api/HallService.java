package service.api;

import dto.HallDto;

import java.util.List;

public interface HallService {

    void create (HallDto hallDto);
    void delete(Integer id);
    List<HallDto> findAll();
    HallDto update(HallDto hallDto);
    HallDto findById(Integer id);
}
