package service.api;

import dto.TicketDto;
import java.util.List;

public interface TicketService{
    void create (TicketDto ticketDto);
    void delete(Integer id);
    List<TicketDto> findAll();
    TicketDto update(TicketDto ticketDto);
    TicketDto findById(Integer id);
    void deleteAllTicketsBySessionId(Integer sessionId);
    List<TicketDto> findAllTicketsBySessionId(Integer sessionId);
}
