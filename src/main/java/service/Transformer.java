package service;

import dto.*;
import model.*;

import java.util.LinkedList;
import java.util.List;

public class Transformer {

    public static List<MovieDto> listMovieToListMovieDTO(List<Movie> movies) {
        List<MovieDto> movieDTOs = new LinkedList<MovieDto>();
        for (Movie movie : movies) {
            MovieDto movieDto = movieToMovieDto(movie);
            movieDTOs.add(movieDto);
        }
        return movieDTOs;
    }

    public static MovieDto movieToMovieDto(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setTitle(movie.getTitle());
        movieDto.setGenre(movie.getGenre());
        movieDto.setDescription(movie.getDescription());
        movieDto.setDuration(movie.getDuration());
        return movieDto;
    }

    public static Movie movieDtoToMovie(MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setId(movieDto.getId());
        movie.setTitle(movieDto.getTitle());
        movie.setGenre(movieDto.getGenre());
        movie.setDescription(movieDto.getDescription());
        movie.setDuration(movieDto.getDuration());
        return movie;
    }


    public static List<TicketDto> listTicketToListTocketDto(List<Ticket> tickets) {
        List<TicketDto> ticketDtos = new LinkedList<TicketDto>();
        for (Ticket ticket : tickets) {
            TicketDto ticketDto = ticketToTicketDto(ticket);
            ticketDtos.add(ticketDto);
        }
        return ticketDtos;
    }

    public static List<Ticket> listTicketDtoToListTocket(List<TicketDto> ticketDtos) {
        List<Ticket> tickets = new LinkedList<Ticket>();
        for (TicketDto ticketDto : ticketDtos) {
            Ticket ticket = ticketDtoToTicket(ticketDto);
            tickets.add(ticket);
        }
        return tickets;
    }

    public static TicketDto ticketToTicketDto(Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setPlace(ticket.getPlace());
        ticketDto.setRow(ticket.getRow());
        ticketDto.setUserDto(userToUserDto(ticket.getUser()));
        ticketDto.setSessionDto(sessionToSessionDto(ticket.getSession()));
        return ticketDto;
    }

    public static Ticket ticketDtoToTicket(TicketDto ticketDto)  {
        Ticket ticket = new Ticket();
        ticket.setId(ticketDto.getId());
        ticket.setPlace(ticketDto.getPlace());
        ticket.setRow(ticketDto.getRow());
        ticket.setUser(userDtoToUser(ticketDto.getUserDto()));
        ticket.setSession(sessionDtoToSession(ticketDto.getSessionDto()));
        return ticket;
    }


    public static List<UserDto> listUserToListUserDto(List<User> users) {
        List<UserDto> userDtos = new LinkedList<UserDto>();
        for (User user : users) {
            UserDto userDto = userToUserDto(user);
            userDtos.add(userDto);
        }
        return userDtos;
    }

    public static UserDto userToUserDto(User user) {
        UserDto userDto = new UserDto();
        if(user == null) return null;
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setBirthday(user.getBirthday());
        userDto.setEmail(user.getEmail());
        userDto.setRole(user.getRole());
        return userDto;
    }

    public static User userDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setBirthday(userDto.getBirthday());
        user.setEmail(userDto.getEmail());
        user.setRole(userDto.getRole());
        return user;

    }


    public static List<SessionDto> listSessionToListSessionDto(List<Session> sessions) {
        List<SessionDto> sessionDtos = new LinkedList<SessionDto>();
        for (Session session : sessions) {
            SessionDto sessionDto = sessionToSessionDto(session);
            sessionDtos.add(sessionDto);
        }
        return sessionDtos;
    }

    public static SessionDto sessionToSessionDto(Session session) {
        SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setMovieDto(movieToMovieDto(session.getMovie()));
        sessionDto.setSessionDate(session.getSessionDate());
        sessionDto.setHallDto(hallToHallDto(session.getHall()));
        return sessionDto;
    }

    public static Session sessionDtoToSession(SessionDto sessionDto) {
        Session session = new Session();
        session.setId(sessionDto.getId());
        session.setMovie(movieDtoToMovie(sessionDto.getMovieDto()));
        session.setSessionDate(sessionDto.getSessionDate());
        session.setHall(hallDtoToHall(sessionDto.getHallDto()));
        return session;
    }


    public static List<HallDto> listHallsToListHallsDto(List<Hall> halls) {
        List<HallDto> hallDTOs = new LinkedList<HallDto>();
        for (Hall hall : halls) {
            HallDto hallDto = hallToHallDto(hall);
            hallDTOs.add(hallDto);
        }
        return hallDTOs;
    }

    public static HallDto hallToHallDto(Hall hall) {
        HallDto hallDto = new HallDto();
        hallDto.setId(hall.getId());
        hallDto.setName(hall.getName());
        hallDto.setRowCount(hall.getRowCount());
        hallDto.setPlaceCount(hall.getPlaceCount());
        return hallDto;
    }

    public static Hall hallDtoToHall(HallDto hallDto) {
        Hall hall = new Hall();
        hall.setId(hallDto.getId());
        hall.setName(hallDto.getName());
        hall.setRowCount(hallDto.getRowCount());
        hall.setPlaceCount(hallDto.getPlaceCount());
        return hall;
    }

}
