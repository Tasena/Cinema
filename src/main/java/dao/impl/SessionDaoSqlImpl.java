package dao.impl;


import dao.api.SessionDao;
import dataSource.DataSource;
import model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class SessionDaoSqlImpl extends DaoMysql implements SessionDao{

    private static SessionDaoSqlImpl sessionDao;
    private DataSource dataSource;
    private final String FIND_BY_MOVIE_ID = "SELECT * FROM SESSION WHERE MOVIE_ID = ?";

    public static synchronized SessionDaoSqlImpl getInstance() {
        if (sessionDao == null) {
            sessionDao = new SessionDaoSqlImpl(Session.class);
        }
        return sessionDao;
    }

    public SessionDaoSqlImpl(Class type) {
        super(type);
    }

    @Override
    public List<Session> findAllSessionsByMovieId(Integer movieId) {
        DataSource ds = DataSource.getInstance();
        List<Session> sessions = new LinkedList<>();
        try {
            Session session = null;
            PreparedStatement preparedStatement = ds.getConnection().prepareStatement(FIND_BY_MOVIE_ID);
            preparedStatement.setInt(1, movieId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                session = new Session();
                session.setId(resultSet.getInt(1));
                java.util.Date date = new java.util.Date(resultSet.getTimestamp(2).getTime());
                session.setSessionDate(date);
                MovieDaoSqlImpl movieDaoSql = MovieDaoSqlImpl.getInstance();
                session.setMovie((Movie) movieDaoSql.findById(resultSet.getInt(4)));
                HallDaoSqlImpl hallDaoSql = HallDaoSqlImpl.getInstance();
                session.setHall((Hall) hallDaoSql.findById(resultSet.getInt(3)));
                sessions.add(session);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sessions;
    }
}
