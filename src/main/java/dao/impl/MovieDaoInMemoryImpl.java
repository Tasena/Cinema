package dao.impl;

import dao.api.MovieDao;
import model.Movie;

public final class MovieDaoInMemoryImpl extends DaoInMemory implements MovieDao{

    private static MovieDaoInMemoryImpl movieDaoInMemory;

    public MovieDaoInMemoryImpl(Class existClass) {
        super(existClass);
    }

    public static synchronized MovieDaoInMemoryImpl getInstance() {
        if (movieDaoInMemory == null) {
            movieDaoInMemory = new MovieDaoInMemoryImpl(Movie.class);
        }
        return movieDaoInMemory;
    }
}
