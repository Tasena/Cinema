package dao.impl;

import dao.storage.InMemoryDb;
import dao.api.GenericCrudDao;
import java.util.List;

public class DaoInMemory<T> implements GenericCrudDao<T> {

    private Class<T> existClass;

    public DaoInMemory(Class<T> existClass) {
        this.existClass = existClass;
    }

    public void create(T t) {
    InMemoryDb.getInstance().create(t);
    }

    public List<T> findAll(){
        Class entity = getExistClass();
        return InMemoryDb.getInstance().findAll(entity);
    }

    public T update(T t) {
         return (T) InMemoryDb.getInstance().update(t);

    }

    public T findById(Integer id) {
        Class entity = getExistClass();
        return (T) InMemoryDb.getInstance().findById(entity, id);
    }

    public void delete(Integer id) {
        Class entity = getExistClass();
        InMemoryDb.getInstance().delete(entity, id);
    }


    public Class<T> getExistClass() {
        return existClass;
    }

    public void setExistClass(Class<T> existClass) {
        this.existClass = existClass;
    }
}
