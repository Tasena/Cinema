package dao.impl;

import dao.api.GenericCrudDao;
import dataSource.DataSource;
import model.*;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DaoMysql<T> implements GenericCrudDao<T> {

    private Class<T> type;
    private DataSource dataSource;
    private ResultSet resultSet;
    private Movie movie;
    private User user;
    private Session session;
    private Hall hall;
    private Ticket ticket;
    private static Map<Object, String> stringMap = new HashMap<Object, String>();

    public static final String MOVIE = "Movie";
    public static final String USER= "User";
    public static final String SESSION = "Session";
    public static final String HALL = "Hall";
    public static final String TICKET = "Ticket";

    private final String FIND_ALL = "SELECT * FROM %s";
    private final String CREATE = "INSERT INTO %s %s";
    private final String DELETE = "DELETE FROM %s WHERE ID = ?";

    private final String UPDATE_MOVIE = "UPDATE MOVIE SET TITLE=?, GENRE=?, DESCRIPTION=?, DURATION=? WHERE ID=?";
    private final String UPDATE_USER = "UPDATE USER SET FIRST_NAME=?, LAST_NAME=?, LOGIN=?, PASSWORD=?," +
            "BIRTHDAY=?, EMAIL=?, ROLE=? WHERE ID=?";
    private final String UPDATE_SESSION = "UPDATE MOVIE SET DATE=?, HALL_ID=?, MOVIE_ID=? WHERE ID=?";
    private final String UPDATE_HALL = "UPDATE HALL SET NAME=?, ROW_COUNT=?, PLACE_COUNT=? WHERE ID=?";
    private final String UPDATE_TICKET = "UPDATE TICKET SET PLACE=?, ROW=?, USER_ID=?, SESSION_ID=? WHERE ID=?";
    private final String FIND_BY_ID = "SELECT * FROM %S WHERE ID = ?";

    public DaoMysql(Class<T> type) {
        this.type = type;
        dataSource =DataSource.getInstance();
        stringMap.put(Movie.class, MOVIE);
        stringMap.put(User.class, USER);
        stringMap.put(Session.class, SESSION);
        stringMap.put(Hall.class, HALL);
        stringMap.put(Ticket.class, TICKET);

    }

    public void create(T entity) {

        Connection connection = dataSource.getConnection();
        try {
            PreparedStatement preparedStatement = null;
            String sql = null;
            switch (stringMap.get(type)) {
                case MOVIE:
                    sql = String.format(CREATE, stringMap.get(type), "(title, genre, description, duration) values " +
                            "(?, ?, ?, ?)");
                    preparedStatement = connection.prepareStatement(sql);
                    movie = (Movie) entity;
                    preparedStatement.setString(1, movie.getTitle());
                    preparedStatement.setString(2, movie.getGenre());
                    preparedStatement.setString(3, movie.getDescription());
                    preparedStatement.setInt(4, movie.getDuration());
                    preparedStatement.execute();
                    break;


                case USER:
                    sql = String.format(CREATE, stringMap.get(type), "(first_name, last_name, login, password, birthday, email, role)" +
                            " values (?, ?, ?, ?, ?, ?, ?)");
                    preparedStatement = connection.prepareStatement(sql);
                    user = (User) entity;
                    preparedStatement.setString(1, user.getFirstName());
                    preparedStatement.setString(2, user.getLastName());
                    preparedStatement.setString(3, user.getLogin());
                    preparedStatement.setString(4, user.getPassword());
                    preparedStatement.setDate(5, java.sql.Date.valueOf(user.getBirthday()));
                    preparedStatement.setString(6, user.getEmail());
                    preparedStatement.setString(7, String.valueOf(user.getRole()));
                    preparedStatement.execute();
                    break;

                case SESSION:
                    sql = String.format(CREATE, stringMap.get(type), "(date, hall_id, movie_id) values (?, ?, ?)");
                    preparedStatement = connection.prepareStatement(sql);
                    session = (Session) entity;
                    java.util.Date date = session.getSessionDate();
                    preparedStatement.setDate(1, new java.sql.Date(date.getTime()));// look more
                    preparedStatement.setInt(2, session.getHall().getId());
                    preparedStatement.setInt(3, session.getMovie().getId());
                    preparedStatement.execute();
                    break;

                case HALL :
                    sql = String.format(CREATE, stringMap.get(type), "(name, row_count, place_count) values (?, ?, ?)");
                    preparedStatement = connection.prepareStatement(sql);
                    hall = (Hall) entity;
                    preparedStatement.setString(1, hall.getName());
                    preparedStatement.setInt(2, hall.getRowCount());
                    preparedStatement.setInt(3, hall.getPlaceCount());
                    preparedStatement.execute();
                    break;

                case TICKET :
                    sql = String.format(CREATE, stringMap.get(type), "(place, row, user_id, session_id) values (?, ?, ?, ?)");
                    preparedStatement = connection.prepareStatement(sql);
                    ticket = (Ticket) entity;
                    preparedStatement.setInt(1, ticket.getPlace());
                    preparedStatement.setInt(2, ticket.getRow());
                    preparedStatement.setInt(3, ticket.getUser().getId());
                    preparedStatement.setInt(4, ticket.getSession().getId());
                    preparedStatement.execute();
                    break;

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void delete(Integer id) {
        Connection connection = dataSource.getConnection();
        try {
            String sql = String.format(DELETE, stringMap.get(type));
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<T> findAll(){
        Connection connection = dataSource.getConnection();
        List<T> list = new LinkedList<>();
        try {
            String sql = String.format(FIND_ALL, stringMap.get(type));
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            switch (stringMap.get(type)) {
                case MOVIE:
                    movie = null;
                    while (resultSet.next()) {
                        movie = new Movie();
                        resultSetMovie();
                        list.add((T)movie);
                    }
                    return list;

                case USER :
                    user = null;
                    while (resultSet.next()) {
                        user = new User();
                        resultSetUser();
                        list.add((T)user);
                    }
                    return list;

                case SESSION :
                    session = null;
                    while (resultSet.next()) {
                        session = new Session();
                        resultSetSession();
                        list.add((T)session);
                    }
                    return list;

                case HALL :
                    hall = null;
                    while (resultSet.next()) {
                        hall = new Hall();
                        resultSetHall();
                        list.add((T)hall);
                    }
                    return list;

                case TICKET :
                    ticket = null;
                    while (resultSet.next()) {
                        ticket = new Ticket();
                        resultSetTicket();
                        list.add((T)ticket);
                    }
                    return list;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public T update(T entity){
        T t = null;
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;
        String sql = null;
        try {
        switch (stringMap.get(type)) {
            case MOVIE :
                movie = (Movie) entity;
                preparedStatement = connection.prepareStatement(UPDATE_MOVIE);
                preparedStatement.setString(1, movie.getTitle());
                preparedStatement.setString(2, movie.getGenre());
                preparedStatement.setString(3, movie.getDescription());
                preparedStatement.setInt(4, movie.getDuration());
                preparedStatement.setInt(5, movie.getId());
                preparedStatement.executeUpdate();
                t = (T) movie;
                break;

            case USER :
                user = (User) entity;
                preparedStatement = connection.prepareStatement(UPDATE_USER);
                preparedStatement.setString(1, user.getFirstName());
                preparedStatement.setString(2, user.getLastName());
                preparedStatement.setString(3, user.getLogin());
                preparedStatement.setString(4, user.getPassword());
                preparedStatement.setDate(5, java.sql.Date.valueOf(user.getBirthday()));
                preparedStatement.setString(6, user.getEmail());
                preparedStatement.setString(7, String.valueOf(user.getRole()));
                preparedStatement.setInt(8, user.getId());
                preparedStatement.executeUpdate();
                t =  (T) user;
                break;

            case SESSION :
                session = (Session) entity;
                preparedStatement = connection.prepareStatement(UPDATE_SESSION);
                java.util.Date date = session.getSessionDate();
                preparedStatement.setDate(1, new java.sql.Date(date.getTime()));// look more
                preparedStatement.setInt(2, session.getHall().getId());
                preparedStatement.setInt(3, session.getMovie().getId());
                preparedStatement.setInt(4, session.getId());
                preparedStatement.executeUpdate();
                t =  (T) session;
                break;

            case HALL :
                hall = (Hall) entity;
                preparedStatement = connection.prepareStatement(UPDATE_HALL);
                preparedStatement.setString(1, hall.getName());
                preparedStatement.setInt(2, hall.getRowCount());
                preparedStatement.setInt(3, hall.getPlaceCount());
                preparedStatement.setInt(4, hall.getId());
                preparedStatement.executeUpdate();
                t = (T) hall;
                break;

            case TICKET :
                ticket = (Ticket) entity;
                preparedStatement = connection.prepareStatement(UPDATE_TICKET);
                preparedStatement.setInt(1, ticket.getPlace());
                preparedStatement.setInt(2, ticket.getRow());
                preparedStatement.setInt(3, ticket.getUser().getId());
                preparedStatement.setInt(4, ticket.getSession().getId());
                preparedStatement.setInt(5, ticket.getId());
                preparedStatement.executeUpdate();
                t =  (T) ticket;

                }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return t;
    }

    public T findById(Integer id) {
        Connection connection = dataSource.getConnection();
         T entity = null;
        try {
            String sql = String.format(FIND_BY_ID, stringMap.get(type));
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            switch (stringMap.get(type)) {
                case MOVIE:
                    movie = null;
                    preparedStatement.setInt(1, id);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        movie = new Movie();
                        resultSetMovie();
                        return (T) movie;
                    }

                case USER:
                    user = null;
                    preparedStatement.setInt(1, id);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        user = new User();
                        resultSetUser();
                        return  (T) user;
                    }

                case SESSION :
                    session = null;
                    preparedStatement.setInt(1, id);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        session = new Session();
                        resultSetSession();
                        return (T) session;
                    }

                case HALL :
                    hall = null;
                    preparedStatement.setInt(1, id);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        hall = new Hall();
                        resultSetHall();
                        return  (T) hall;
                    }

                case TICKET :
                    ticket = null;
                    preparedStatement.setInt(1, id);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        ticket = new Ticket();
                        resultSetTicket();
                        return (T) ticket;
                    }
            }
        } catch (SQLException e) {
                    e.printStackTrace();
                }
        return entity;
    }

    private void resultSetMovie() throws SQLException {
        movie.setId(resultSet.getInt(1));
        movie.setTitle(resultSet.getString(2));
        movie.setGenre(resultSet.getString(3));
        movie.setDescription(resultSet.getString(4));
        movie.setDuration(resultSet.getInt(5));
    }

    private void resultSetUser() throws SQLException{
        user.setId(resultSet.getInt(1));
        user.setFirstName(resultSet.getString(2));
        user.setLastName(resultSet.getString(3));
        user.setLogin(resultSet.getString(4));
        user.setPassword(resultSet.getString(5));
        user.setBirthday(resultSet.getDate(6).toLocalDate());
        user.setEmail(resultSet.getString(7));
        user.setRole(UserRole.valueOf(resultSet.getString(8)));
    }

    private void resultSetSession() throws SQLException {
        session.setId(resultSet.getInt(1));
        java.util.Date date = new java.util.Date(resultSet.getTimestamp(2).getTime());
        session.setSessionDate(date);
        MovieDaoSqlImpl movieDaoSql = MovieDaoSqlImpl.getInstance();
        session.setMovie((Movie) movieDaoSql.findById(resultSet.getInt(4)));
        HallDaoSqlImpl hallDaoSql = HallDaoSqlImpl.getInstance();
        session.setHall((Hall) hallDaoSql.findById(resultSet.getInt(3)));
    }

    private void resultSetHall() throws SQLException {
        hall.setId(resultSet.getInt(1));
        hall.setName(resultSet.getString(2));
        hall.setRowCount(resultSet.getInt(3));
        hall.setPlaceCount(resultSet.getInt(4));
    }

    private void resultSetTicket() throws SQLException {
        ticket.setId(resultSet.getInt(1));
        ticket.setPlace(resultSet.getInt(2));
        ticket.setRow(resultSet.getInt(3));
        ticket.setUser((User)findById(resultSet.getInt(4)));
        ticket.setSession((Session) findById(resultSet.getInt(5)));
    }
}
