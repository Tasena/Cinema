package dao.impl;

import dao.api.MovieDao;
import model.Movie;


public class MovieDaoSqlImpl extends DaoMysql implements MovieDao {

    private static MovieDaoSqlImpl movieDao;

    public static synchronized MovieDaoSqlImpl getInstance() {
        if (movieDao == null) {
            movieDao = new MovieDaoSqlImpl(Movie.class);
        }
        return movieDao;
    }

    public MovieDaoSqlImpl(Class type) {
        super(type);
    }
}
