package dao.impl;

import dao.api.TicketDao;
import dao.storage.InMemoryDb;
import model.Ticket;

import java.util.List;

public final class TicketDaoInMemoryImpl extends DaoInMemory implements TicketDao{

    private static TicketDaoInMemoryImpl ticketDaoInMemory;

    public TicketDaoInMemoryImpl(Class existClass) {
        super(existClass);
    }

    public static synchronized TicketDaoInMemoryImpl getInstance() {
        if (ticketDaoInMemory == null) {
            ticketDaoInMemory = new TicketDaoInMemoryImpl(Ticket.class);
        }
        return ticketDaoInMemory;
    }

    public void deleteAllTicketsBySessionId(Integer sessionId) {
        InMemoryDb.getInstance().deleteAllTicketsBySessionId(sessionId);
    }

    public List<Ticket> findAllTicketsBySessionId(Integer sessionId) {
        return InMemoryDb.getInstance().findAllTicketsBySessionId(sessionId);
    }
}
