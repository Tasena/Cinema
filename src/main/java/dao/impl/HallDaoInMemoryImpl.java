package dao.impl;

import dao.api.HallDao;
import model.Hall;

public final class HallDaoInMemoryImpl extends DaoInMemory implements HallDao{

    private static HallDaoInMemoryImpl hallDaoInMemory;

    public static synchronized HallDaoInMemoryImpl getInstance() {
        if (hallDaoInMemory == null) {
            hallDaoInMemory = new HallDaoInMemoryImpl(Hall.class);
        }
        return hallDaoInMemory;
    }


    public HallDaoInMemoryImpl(Class existClass) {
        super(existClass);
    }
}
