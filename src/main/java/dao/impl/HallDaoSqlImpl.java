package dao.impl;


import dao.api.HallDao;
import model.Hall;

public final class HallDaoSqlImpl extends DaoMysql implements HallDao{

    private static HallDaoSqlImpl hallDao;

    public static synchronized HallDaoSqlImpl getInstance() {
        if (hallDao == null) {
            hallDao = new HallDaoSqlImpl(Hall.class);
        }
        return hallDao;
    }

    public HallDaoSqlImpl(Class type) {
        super(type);
    }
}
