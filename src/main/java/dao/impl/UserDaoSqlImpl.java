package dao.impl;

import dao.api.UserDao;
import dataSource.DataSource;
import model.User;
import model.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoSqlImpl extends DaoMysql implements UserDao{

    private static UserDaoSqlImpl userDao;
    private final String FIND_BY_LOGIN_PASSWORD = "SELECT * FROM USER WHERE LOGIN = ? AND PASSWORD = ?";
    private DataSource dataSource;

    public static synchronized UserDaoSqlImpl getInstance() {
        if (userDao == null) {
            userDao = new UserDaoSqlImpl(User.class);
        }
        return userDao;
    }


    public UserDaoSqlImpl(Class type) {
        super(type);
    }

    @Override
    public User findUserByLoginPassword(String login, String password) {
        DataSource ds = DataSource.getInstance();
        User user = null;
        try {
            PreparedStatement preparedStatement = ds.getConnection().prepareStatement(FIND_BY_LOGIN_PASSWORD);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(1));
                user.setFirstName(resultSet.getString(2));
                user.setLastName(resultSet.getString(3));
                user.setLogin(resultSet.getString(4));
                user.setPassword(resultSet.getString(5));
                user.setBirthday(resultSet.getDate(6).toLocalDate());
                user.setEmail(resultSet.getString(7));
                user.setRole(UserRole.valueOf(resultSet.getString(8)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}
