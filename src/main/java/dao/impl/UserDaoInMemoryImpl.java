package dao.impl;

import dao.api.UserDao;
import dao.storage.InMemoryDb;
import model.User;

public final class UserDaoInMemoryImpl extends DaoInMemory implements UserDao{

    private static UserDaoInMemoryImpl userDaoInMemory;

    public static synchronized UserDaoInMemoryImpl getInstance() {
        if (userDaoInMemory == null) {
            userDaoInMemory = new UserDaoInMemoryImpl(User.class);
        }
        return userDaoInMemory;
    }

    public UserDaoInMemoryImpl(Class existClass) {
        super(existClass);
    }

    public User findUserByLoginPassword(String login, String password) {
        return InMemoryDb.getInstance().findUserByLoginPassword(login, password);
    }
}
