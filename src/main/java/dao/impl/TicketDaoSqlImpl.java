package dao.impl;


import dao.api.TicketDao;
import dataSource.DataSource;
import model.Session;
import model.Ticket;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TicketDaoSqlImpl extends DaoMysql implements TicketDao{

    private static TicketDaoSqlImpl ticketDaoSql;
    private final String DELETE_BY_SESSION_ID = "DELETE * FROM TICKET WHERE SESSION_ID = ?";
    private final String FIND_BY_SESSION_ID = "DELETE * FROM TICKET WHERE SESSION_ID = ?";
    private DataSource dataSource;

    public static synchronized TicketDaoSqlImpl getInstance() {
        if (ticketDaoSql == null) {
            ticketDaoSql = new TicketDaoSqlImpl(Ticket.class);
        }
        return ticketDaoSql;
    }

    public TicketDaoSqlImpl(Class type) {
        super(type);
    }

    @Override
    public void deleteAllTicketsBySessionId(Integer sessionId) {
        Connection connection = dataSource.getConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(DELETE_BY_SESSION_ID);
            ps.setInt(1, sessionId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Ticket> findAllTicketsBySessionId(Integer sessionId) {
        Connection connection = dataSource.getConnection();
        List<Ticket> tickets = new LinkedList<>();
        try {
            Ticket ticket = null;
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_SESSION_ID);
            preparedStatement.setInt(1, sessionId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ticket = new Ticket();
                ticket.setId(resultSet.getInt(1));
                ticket.setPlace(resultSet.getInt(2));
                ticket.setRow(resultSet.getInt(3));
                ticket.setUser((User)findById(resultSet.getInt(4)));
                ticket.setSession((Session) findById(resultSet.getInt(5)));
                tickets.add(ticket);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tickets;
    }
}
