package dao.impl;

import dao.api.SessionDao;
import dao.storage.InMemoryDb;
import model.Session;

import java.util.Date;
import java.util.List;


public final class SessionDaoInMemoryImpl extends DaoInMemory implements SessionDao{


    private static SessionDaoInMemoryImpl sessionDaoInMemory;

    public static synchronized SessionDaoInMemoryImpl getInstance() {
        if (sessionDaoInMemory == null) {
            sessionDaoInMemory = new SessionDaoInMemoryImpl(Session.class);
        }
        return sessionDaoInMemory;
    }
    public SessionDaoInMemoryImpl(Class existClass) {
        super(existClass);
    }


    public List<Session> findAllSessionsByMovieId(Integer movieId) {
        return InMemoryDb.getInstance().findAllSessionsByMovieId(movieId);
    }
}
