package dao.storage;

import model.*;

import java.util.LinkedList;
import java.util.List;


public final class InMemoryDb<T> {

    private InMemoryDb() {
    }

    private static InMemoryDb inMemoryDb;
    private List<Movie> movies = new LinkedList<Movie>();
    private List<Session> sessions = new LinkedList<Session>();
    private List<Hall> halls = new LinkedList<Hall>();
    private List<User> users = new LinkedList<User>();
    private List<Ticket> tickets = new LinkedList<Ticket>();

    private int userIdsCounter = 0;
    private int movieIdsCounter = 0;
    private int sessionIdsCounter = 0;
    private int ticketIdsCounter = 0;
    private int hallIdsCounter = 0;

    public static InMemoryDb getInstance() {
        if (inMemoryDb == null) {
            synchronized (InMemoryDb.class) {
                if (inMemoryDb == null) {
                    inMemoryDb = new InMemoryDb();
                }
            }
        }
        return inMemoryDb;
    }

    public List<T> findAll(Class<T> t) {
        List<T> list = new LinkedList<T>();
        if(t.equals(Movie.class)) list = (List<T>) movies;
        else if(t.equals(User.class)) list = (List<T>) users;
        else if(t.equals(Ticket.class)) list = (List<T>) tickets;
        else if(t.equals(Session.class)) list = (List<T>) sessions;
        else if(t.equals(Hall.class)) list = (List<T>) halls;
        return list;
    }

    public void create (T t) {
        if(t instanceof Movie) {
            ((Movie)t).setId(++movieIdsCounter);
            movies.add((Movie)t);
        }
        if(t instanceof User) {
            ((User)t).setId(++userIdsCounter);
            users.add((User)t);
        }

        if(t instanceof Ticket) {
            ((Ticket)t).setId(++ticketIdsCounter);
            tickets.add((Ticket) t);
        }

        if(t instanceof Hall) {
            ((Hall)t).setId(++hallIdsCounter);
            halls.add((Hall) t);
        }

        if(t instanceof Session) {
            ((Session)t).setId(++sessionIdsCounter);
            sessions.add((Session) t);
        }
    }

    public void delete(Class<T> t, Integer id) {
        if(t.equals(Movie.class)){
        movies.remove(findById(t, id));
        }

        if(t.equals(User.class)){
            users.remove(findById(t, id));
        }

        if(t.equals(Ticket.class)){
            tickets.remove(findById(t, id));
        }

        if(t.equals(Session.class)){
            sessions.remove(findById(t, id));
        }
        if(t.equals(Hall.class)){
            halls.remove(findById(t, id));
        }
    }

    public T findById(Class<T> t, Integer id) {
        if(t.equals(Movie.class)) {
            for (Movie movie : movies) {
                if (id.equals(movie.getId())) {
                    return (T) movie;
                }
            }
        }
        if(t.equals(User.class)) {
            for (User user : users) {
                if (id.equals(user.getId())) {
                    return (T) user;
                }
            }
        }

        if(t.equals(Hall.class)) {
            for (Hall hall : halls) {
                if (id.equals(hall.getId())) {
                    return (T) hall;
                }
            }
        }

        if(t.equals(Ticket.class)) {
            for (Ticket ticket : tickets) {
                if (id.equals(ticket.getId())) {
                    return (T) ticket;
                }
            }
        }

        if(t.equals(Session.class)) {
            for (Session session : sessions) {
                if (id.equals(session.getId())) {
                    return (T) session;
                }
            }
        }
        return null;
    }

    public T update(T t) {
        T obj = null;
        if(t instanceof Movie) {
            for (Movie movie : movies) {
                if(movie.getId().equals(((Movie)t).getId())) {
                    movie.setTitle(((Movie) t).getTitle());
                    movie.setGenre(((Movie) t).getGenre());
                    movie.setDescription(((Movie) t).getDescription());
                    movie.setDuration(((Movie) t).getDuration());
                    obj = (T)movie;
                }
            }
        }

        if(t instanceof Session) {
            for (Session session : sessions) {
                if(session.getId().equals(((Session)t).getId())) {
                    session.setMovie(((Session) t).getMovie());
                    session.setHall(((Session) t).getHall());
                    session.setSessionDate(((Session) t).getSessionDate());
                    obj = (T)session;
                }
            }
        }

        if(t instanceof User) {
            for (User user : users) {
                if(user.getId().equals(((User)t).getId())) {
                   user.setFirstName(((User) t).getFirstName());
                    user.setLastName(((User) t).getLastName());
                    user.setLogin(((User) t).getLogin());
                    user.setPassword(((User) t).getPassword());
                    user.setEmail(((User) t).getEmail());
                    user.setBirthday(((User) t).getBirthday());
                    user.setRole(((User) t).getRole());
                    obj = (T)user;
                }

            }
        }

        if(t instanceof Hall) {
            for (Hall hall : halls) {
                if(hall.getId().equals(((Hall)t).getId())) {
                    hall.setName(((Hall) t).getName());
                    hall.setRowCount(((Hall) t).getPlaceCount());
                    hall.setRowCount(((Hall) t).getRowCount());
                    obj = (T)hall;
                }
            }
        }

        if(t instanceof Ticket) {
            for (Ticket ticket : tickets) {
                if(ticket.getId().equals(((Ticket)t).getId())) {
                    ticket.setRow(((Ticket) t).getRow());
                    ticket.setPlace(((Ticket) t).getPlace());
                    ticket.setUser(((Ticket) t).getUser());
                    ticket.setSession(((Ticket) t).getSession());
                    obj = (T)ticket;
                }
            }
        }
        return obj;
    }

    public List<Session> findAllSessionsByMovieId(Integer movieId) {
        List<Session> session1 = new LinkedList<Session>();
        for (Session session : sessions) {
            if(session.getMovie().getId().equals(movieId))
                session1.add(session);
        }
        return session1;
    }

   public void deleteAllTicketsBySessionId(Integer sessionId) {
       tickets.remove(findAllTicketsBySessionId(sessionId));
   }

    public List<Ticket> findAllTicketsBySessionId(Integer sessionId) {
        List<Ticket> ticket1 = new LinkedList<>();
        for (Ticket ticket : tickets) {
            if(ticket.getSession().getId().equals(sessionId)) {
                ticket1.add(ticket);
            }
        }
        return ticket1;
    }

    public User findUserByLoginPassword(String login, String password){
        User user = null;
        for (User user1 : users) {
            if(user1.getLogin().equals(login) && user1.getPassword().equals(password))
                user = user1;
            return user;
        }
        return user;
    }
}
