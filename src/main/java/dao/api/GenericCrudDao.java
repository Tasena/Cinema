package dao.api;

import java.util.List;

public interface GenericCrudDao<T> {
    void create (T t);
    void delete(Integer id);
    List<T> findAll();
    T update(T t);
    T findById(Integer id);
}
