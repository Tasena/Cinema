package dao.api;

import model.Ticket;
import java.util.List;


public interface TicketDao extends GenericCrudDao {

    void deleteAllTicketsBySessionId(Integer sessionId);
    List<Ticket> findAllTicketsBySessionId(Integer sessionId);
}
