package dao.api;

import model.User;

public interface UserDao extends GenericCrudDao{

    User findUserByLoginPassword(String login, String password);
}
