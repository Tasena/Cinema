package dao.api;

import model.Session;

import java.util.List;


public interface SessionDao extends GenericCrudDao {

    List<Session> findAllSessionsByMovieId(Integer movieId);
}
