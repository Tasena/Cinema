package model;

public class Movie {
    private Integer id;
    private String title;
    private String genre;
    private String description;
    private Integer duration;

    public Movie() {
    }

    public Movie(Integer id, String title, String genre, String description, Integer duration) {
        setId(id);
        setTitle(title);
        setGenre(genre);
        setDescription(description);
        setDuration(duration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return getId().equals(movie.getId()) && getTitle().equals(movie.getTitle()) && getDescription().equals(movie.getDescription());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getGenre().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getDuration().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
         this.genre = genre;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
