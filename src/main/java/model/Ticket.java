package model;

import java.io.Serializable;

public class Ticket implements Serializable {

    private Integer id;
    private Integer place;
    private Integer row;
    private User user;
    private Session session;

    public Ticket() {
    }

    public Ticket(Integer id, Integer place, Integer row, User user, Session session) {
        setId(id);
        setPlace(place);
        setRow(row);
        setUser(user);
        setSession(session);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        if (!getId().equals(ticket.getId())) return false;
        if (!getPlace().equals(ticket.getPlace())) return false;
        if (!getRow().equals(ticket.getRow())) return false;
        if (!getUser().equals(ticket.getUser())) return false;
        return getSession().equals(ticket.getSession());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getPlace().hashCode();
        result = 31 * result + getRow().hashCode();
        result = 31 * result + getUser().hashCode();
        result = 31 * result + getSession().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", place=" + place +
                ", row=" + row +
                ", user=" + user +
                ", session=" + session +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
