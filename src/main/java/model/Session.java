package model;

import java.util.Date;

public class Session {
    private Integer id;
    private Movie movie;
    private Hall hall;
    private Date sessionDate;

    public Session() {
    }

    public Session(Integer id, Date sessionDate, Movie movie, Hall hall) {
        setId(id);
        setSessionDate(sessionDate);
        setMovie(movie);
        setHall(hall);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return getId().equals(session.getId()) && getMovie().equals(session.getMovie())&& getHall().equals(session.getHall())
        && getSessionDate().equals(session.getSessionDate());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getMovie().hashCode();
        result = 31 * result + getHall().hashCode();
        result = 31 * result + getSessionDate().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", movie=" + movie +
                ", hall=" + hall +
                ", sessionDate=" + sessionDate +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Date getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(Date sessionDate) {
        this.sessionDate = sessionDate;
    }
//при работе приложения необходимо добавить ограничение, чтоб дата сессии была позже текущей даты минимум на 1 день и максимум на 8 дней

}
