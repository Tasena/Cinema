package propertyHolder;



public final class PropertyHolder {

    private static PropertyHolder propertyHolder;
    private final static String propertiesPath = "src\\main\\resources\\application.properties";

    private boolean isDB = true;
    private String jdbcUrl = "jdbc:mysql://localhost:3306/myCinema";
    private String dbUserLogin = "root";
    private String dbUserPassword = "root";
    private String dbDriver = "com.mysql.jdbc.Driver";

    private PropertyHolder() {
    }

    public static synchronized PropertyHolder getInstance() {
        if (propertyHolder == null) {
            propertyHolder = new PropertyHolder();
        }
        return propertyHolder;
    }

    public boolean isDB() {
        return isDB;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public String getDbUserLogin() {
        return dbUserLogin;
    }

    public String getDbUserPassword() {
        return dbUserPassword;
    }

    public String getDbDriver() {
        return dbDriver;
    }
}

