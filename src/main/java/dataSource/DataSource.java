package dataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import propertyHolder.PropertyHolder;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;


public final class DataSource {

    private static ComboPooledDataSource poolConnections;
    private static DataSource dataSource;

    private DataSource() {
        initPollConnections();
    }

    public static synchronized DataSource getInstance() {
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }

    public synchronized Connection getConnection() {
        Connection connection = null;
        try {
            connection = poolConnections.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static synchronized void initPollConnections() {
            poolConnections = new ComboPooledDataSource();
            PropertyHolder propertyHolder = PropertyHolder.getInstance();
            try {
                poolConnections.setDriverClass(propertyHolder.getDbDriver());
                poolConnections.setJdbcUrl(propertyHolder.getJdbcUrl());
                poolConnections.setUser(propertyHolder.getDbUserLogin());
                poolConnections.setPassword(propertyHolder.getDbUserPassword());
                poolConnections.setMinPoolSize(5);
                poolConnections.setAcquireIncrement(3);
                poolConnections.setMaxPoolSize(100);

            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }

    }

}
