INSERT INTO Movie (TITLE, GENRE, DESCRIPTION, DURATION) VALUES ('Captain America: Civil War', 'Action', 
                                                         'In 1991, Hydra operatives in Siberia dispatch the brainwashed Bucky Barnes to intercept an automobile carrying a case of super-soldier serum and assassinate its occupants. Approximately one year after Ultrons defeat at the hands of the Avengers, Steve Rogers, Natasha Romanoff, Sam Wilson, and Wanda Maximoff stop Brock Rumlow from stealing a biological weapon from a lab in Lagos. Rumlow commits suicide with a bomb. Wanda tries to displace the blast into the sky but it devastates a nearby building, killing a number of Wakandan humanitarian workers.',
                                                         147);

INSERT INTO Movie (TITLE, GENRE, DESCRIPTION, DURATION) VALUES ('Ratchet and Clank', 'Animation, Action, Adventure',
                                                         'Above Planet Tenemule, Chairman Drek gives a speech to his race, the Blarg, aboard their new invention, the Deplanitizer, a space station that has the power to blow up planets. They use it on Planet Tenemule, completely destroying it. Cutting to Planet Veldin, Ratchet, a Lombax, is a young mechanic who mysteriously crash-landed on Veldin as an infant, and Grimroth took him under his wing and raised him.',
                                                         94);
INSERT INTO Movie (TITLE, GENRE, DESCRIPTION, DURATION) VALUES ('Miracles from Heaven', ' Drama',
                                                         'Set in Burleson, Texas, in 2011, the film centers on a 10-year-old girl named Anna Beam (Kylie Rogers), daughter of Christy Beam(Jennifer Garner). Anna is suffering from a pseudo-obstruction motility disorder and is unable to eat, using feeding tubes for nutrition. During this ordeal Anna and her mother befriend a local Massachusetts resident. One day, she has a near-death experience, after falling three stories into the hollow of a tree and suffering only scratches. When she is subsequently examined at Boston Children''s Hospital, no evidence of the chronic illness is found. When Anna is later talking to her parents, she claims she went to heaven and talked to God, who sent her back to Earth.',
                                                         109);
INSERT INTO Movie (TITLE, GENRE, DESCRIPTION, DURATION) VALUES ('Mothers Day', ' Comedy, Drama',
                                                         'As Mother''s Day draws close, a group of seemingly unconnected people come to terms with the relationships they have with their mothers. Sandy (Jennifer Aniston) is a divorced mother of two boys whose ex-husband has recently remarried a younger woman named Tina (Shay Mitchell). Miranda (Julia Roberts) is an accomplished writer who gave up her only child, Kristin (Britt Robertson) for adoption at birth. But as a grown-up Kristin prepares herself for marriage, she begins to contemplate the missing part in her life and is encouraged by her friend, Jesse (Kate Hudson), to go out and find her mother. Meanwhile, Jesse, who never sees her mother, is surprised by her parents when they come to visit and must come to terms with their failing relationship.',
                                                         118);

INSERT INTO User (FIRST_NAME, LAST_NAME, LOGIN, PASSWORD, BIRTHDAY, EMAIL, ROLE)
VALUES ('Ivan', 'Ivanov', 'Admin', 'Admin', '1987-01-27', 'dgdfg@gdfg.com', 'ADMIN');

INSERT INTO User (FIRST_NAME, LAST_NAME, LOGIN, PASSWORD, BIRTHDAY, EMAIL, ROLE)
VALUES ('Petr', 'Petrov', 'Petya', 'petya', '1960-05-03', 'petunya@sfsdt.com', 'DEFAULT');

INSERT INTO Hall (NAME, ROW_COUNT, PLACE_COUNT) VALUES ('Red', 10, 12),
  ('Grey', 20, 10),
  ('Black', 5, 3);

INSERT INTO Session (DATE, HALL_ID, MOVIE_ID) VALUES ('2016-08-07 21:30:00', 1, 2),
  ('2016-08-09 12:00:00', 2, 1), ('2016-08-08 15:00:00', 3, 3), ('2016-08-10 19:00:00', 3, 4);

INSERT INTO Ticket (PLACE, ROW, USER_ID, SESSION_ID) VALUES (2, 3, 2, 1), (5, 5, 1, 2);

